/**
 *
 * Based on original LSD-SLAM code from:
 * Copyright 2013 Jakob Engel <engelj at in dot tum dot de> (Technical
 * University of Munich) For more information see
 * <http://vision.in.tum.de/lsdslam>
 *
 * LSD-SLAM is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * LSD-SLAM is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with LSD-SLAM. If not, see <http://www.gnu.org/licenses/>.
 */

#include <boost/thread.hpp>
#include <memory>

#include "ros/ros.h"

#include "g3_to_ros_logger/ROSLogSink.h"
#include "g3_to_ros_logger/g3logger.h"

#include "pangolin_gui_ros/GUI.h"
#include "pangolin_gui_ros/PangolinOutputIOWrapper.h"
#include "pangolin_gui_ros/ROSInputThread.h"
// #include "pangolin_gui_ros/TextOutputIOWrapper.h"

#include "libvideoio/types/Camera.h"

#include "pangolin_gui_ros/Configuration.h"

#include <message_filters/subscriber.h>
#include <message_filters/sync_policies/approximate_time.h>
#include <message_filters/sync_policies/exact_time.h>
#include <message_filters/synchronizer.h>
#include <message_filters/time_synchronizer.h>

#include <dynamic_reconfigure/server.h>

using namespace libvideoio;

typedef message_filters::sync_policies::ApproximateTime<slam_msgs::frameMsg,
                                                        sensor_msgs::Image>
        SyncPolicy;

using namespace lsd_slam;

using std::string;

int main(int argc, char **argv) {
		// Initialize the logging system
		libg3logger::G3Logger<ROSLogSink> logWorker(argv[0]);
		logWorker.logBanner();
		logWorker.verbose(2);

		// init ros
		ros::init(argc, argv, "pangolin_gui");
		ros::NodeHandle nh_("pangolin_gui");

		int slamImageWidth, slamImageHeight;

		float pcSize;

		if (nh_.getParam("/pangolin_gui/input_image/width", slamImageWidth) &&
		    nh_.getParam("/pangolin_gui/input_image/height", slamImageHeight)) {
				ROS_INFO_STREAM("Slam image size is " << slamImageWidth << " x "
				                                      << slamImageHeight);
		} else {
				ROS_FATAL("No image width or height defined");
		}
		int decimation;
		if (nh_.getParam("/pangolin_gui/input_image/decimation", decimation)) {
				ROS_INFO("Decimation : %i ", decimation);
		} else {
				ROS_WARN("No Decimation specified, setting to 1");
				decimation = 1;
		}

		slamImageWidth /= decimation;
		slamImageHeight /= decimation;

		if (nh_.getParam("pointcloud_size", pcSize)) {
				ROS_INFO("Display pointcloud size is: %f", pcSize);
		} else {
				ROS_INFO("Setting pointcloud size to 0.1");
				pcSize = 0.1;
		}

		// Load GUI params
		double scale, scaledTh, absTh, pclLeafSize, pcPointSize;
		int nearSupport, sparisityFactor;
		bool useVoxelFilter, useVarianceFiltering, flipImg;

		double scaleDefault = 2.0;
		double scaledThDefault = 2.0;
		double absThDefault = 2.0;
		int nearSupportDefault = 9;
		int sparisityFactorDefault = 1;
		double pclLeafSizeDefault = 0.01;
		bool useVarianceFilteringDefault = true;
		bool useVoxelFilterDefault = false;
		double pcPointSizeDefault = 1.0;
		bool flipImgDefault = true;

		nh_.param(nh_.resolveName("pointcloud_point_size"), pcPointSize,
		          pcPointSizeDefault);
		nh_.param(nh_.resolveName("variance_filter/pointcloud_scale"), scale, scaleDefault);
		nh_.param(nh_.resolveName("variance_filter/scaled_th"), scaledTh, scaledThDefault);
		nh_.param(nh_.resolveName("abs_th"), absTh, absThDefault);
		nh_.param(nh_.resolveName("near_support"), nearSupport, nearSupportDefault);
		nh_.param(nh_.resolveName("sparisity_factor"), sparisityFactor,
		          sparisityFactorDefault);

		nh_.param("/pangolin_gui/voxel_filter/leaf_size", pclLeafSize, pclLeafSizeDefault);
		nh_.param("/pangolin_gui/voxel_filter/use_voxel_filter", useVoxelFilter,
		          useVoxelFilterDefault);
		nh_.param("/pangolin_gui/variance_filter/use_variance_filter", useVarianceFiltering,
		          useVarianceFilteringDefault);

		nh_.param("flip_debug_image", flipImg, flipImgDefault);

		LOG(INFO) << "slamImageWidth: " << slamImageWidth;

		LOG(INFO) << "use_variance_filter: " << useVarianceFiltering;

		const ImageSize slamImageSize(slamImageWidth, slamImageHeight);

		// Load and set the configuration object
		Conf().setSlamImageSize(slamImageSize);
		libvideoio::Camera camera = Camera(0, 0, 0, 0);

		// GUI need to be initialized in main thread on OSX,
		// so run GUI elements in the main thread.
		GUI gui(Conf().slamImageSize, camera);
		// gui.setRescaleFactor(pcSize);
		Conf().pointcloudSize = pcSize;
		Conf().scale = scale;
		Conf().setScale = true;
		Conf().scaledTh = scaledTh;
		Conf().setscaledTh = true;
		Conf().absTh = absTh;
		Conf().setabsTh = true;
		Conf().nearSupport = nearSupport;
		Conf().setnearSupport = true;
		Conf().sparisityFactor = sparisityFactor;
		Conf().setsparisityFactor = true;

		Conf().printGUIinfo = true;

		Conf().pclLeafSize = pclLeafSize;

		Conf().useVarianceFiltering = useVarianceFiltering;
		Conf().useVoxelFilter = useVoxelFilter;

		std::shared_ptr<PangolinOutputIOWrapper> outputWrapper(
				new PangolinOutputIOWrapper(gui, pcSize));

		ROSInputThread input(outputWrapper, gui);

		std::string debug_img = "/pangolin_gui/debug_image";
		std::string pose_name = "/pangolin_gui/pose";
		std::string graph_name = "/pangolin_gui/graph";
		std::string framePose_name = "/pangolin_gui/framePoses";

		ros::Subscriber debugImgSubscriber = nh_.subscribe<sensor_msgs::Image>(
				debug_img, 1, &ROSInputThread::debugImageCallback, &input);
		ros::Subscriber poseSubscriber = nh_.subscribe<geometry_msgs::PoseStamped>(
				pose_name, 1, &ROSInputThread::poseCallback, &input);
		ros::Subscriber graphSubscriber = nh_.subscribe<slam_msgs::keyframeGraphMsg>(
				graph_name, 1, &ROSInputThread::keyFrameGraphCallback, &input);
		ros::Subscriber framePoseSubscriber = nh_.subscribe<slam_msgs::framePoseData>(
				framePose_name, 1, &ROSInputThread::framePoseDataCallback, &input);

		std::string keyframe_name = "/pangolin_gui/frames";
		std::string img_name = "/pangolin_gui/input_image";

		input.flipDebug = flipImg;

		// // Sync the left image with the disparity map
		message_filters::Subscriber<slam_msgs::frameMsg> keyframe_sub(
				nh_, keyframe_name, 1);
		message_filters::Subscriber<sensor_msgs::Image> img_sub(nh_, img_name, 1);
		//
		message_filters::Synchronizer<SyncPolicy> sync(SyncPolicy(100), keyframe_sub,
		                                               img_sub);

		sync.registerCallback(
				boost::bind(&ROSInputThread::keyFrameImageCallback, &input, _1, _2));

		// Dynamic Reconfigure
		dynamic_reconfigure::Server<pangolin_gui_ros::guiCFGConfig> server;
		dynamic_reconfigure::Server<pangolin_gui_ros::guiCFGConfig>::CallbackType f;

		f = boost::bind(&ROSInputThread::dynamicReconfigureCallback, &input, _1, _2);
		server.setCallback(f);

		ros::ServiceServer service =
				nh_.advertiseService("full_reset", &ROSInputThread::fullReset, &input);

		input.run();

		return 0;
}
