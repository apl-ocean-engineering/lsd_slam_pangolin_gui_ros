/*
 * PangolinOutputIOWrapper.h
 *
 *  Created on: 17 Oct 2014
 *      Author: thomas
 */

#pragma once

#include "GUI.h"

#include "Configuration.h"

// #include "slam_msgs/keyframeGraphMsg.h"
// #include "slam_msgs/keyframeMsg.h"

#include "GuiFrame.h"

#include <pcl/common/transforms.h>
#include <pcl/filters/radius_outlier_removal.h>
#include <pcl/filters/statistical_outlier_removal.h>
#include <pcl/filters/voxel_grid.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl_conversions/pcl_conversions.h>
#include <pcl_ros/point_cloud.h>

typedef pcl::PointXYZRGB PointT;
typedef pcl::PointCloud<PointT> PointCloud;

namespace lsd_slam {

class Frame;
class KeyFrameGraph;

struct GraphConstraint {
  int from;
  int to;
  float err;
};

class PangolinOutputIOWrapper {
public:
  PangolinOutputIOWrapper(GUI &gui);
  PangolinOutputIOWrapper(GUI &gui, float &pointSize);
  ~PangolinOutputIOWrapper();

  void publishPose(const Sophus::Sim3f &pose);

  void updateDepthImage(unsigned char *data);

  // publishes graph and all constraints, as well as updated KF poses.
  void publishTrajectory(std::vector<Eigen::Matrix<float, 3, 1>> trajectory,
                         std::string identifier);

  // Functions only in pangolin OW
  void publishKeyframe(std::shared_ptr<GuiFrame> &frame, const PointCloud &pc,
                       const std::vector<float> idepthvar,
                       const Sophus::Sim3f &pose);

  void updateLiveImage(unsigned char *data);

  void updateKeyframePoses(GraphFramePose *framePoseData, const int num);

  void updateFrameNumber(int id);

  void updateGui();

  void dumpPoints(std::string time_now);

  int publishLvl;

private:
  GUI &_gui;
  float _pointSize;
  float _scale;
};

} // namespace lsd_slam
