/*
 * Keyframe.h
 *
 *  Created on: 17 Oct 2014
 *      Author: thomas
 */

#ifndef KEYFRAME_H_
#define KEYFRAME_H_

#include "sophus/sim3.hpp"
#include <GL/glew.h>
#include <iostream>

#include "Configuration.h"

#include <pcl/filters/voxel_grid.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>

#include <ros/ros.h>

typedef pcl::PointXYZRGB PointT;
typedef pcl::PointCloud<PointT> PointCloud;

struct InputPointDense {
		float xImg;
		float yImg; // xImg, yImg are pixel point locations.
		float x; // x,y are real world locations
		float y;
		float z;
		float depth;
		float idepth_var;
		unsigned char color[3];
};

struct GraphFramePose {
		unsigned int id;
		float camToWorld[7];
};

class Keyframe {
public:
Keyframe()
		: pointData(0), hasVbo(false), points(0), needsUpdate(false),
		pointSize(0.5), scale(10), scaledTh(1e-3), absTh(1e-1), nearSupport(9),
		sparisityFactor(1), _hidden(false) {
}

virtual ~Keyframe() {
		if (pointData)
				delete[] pointData;

		if (hasVbo)
				glDeleteBuffers(1, &vbo);
}

struct MyVertex {
		float point[3];
		unsigned char color[4];
};

void hide() {
		LOG(INFO) << "hide";
		_hidden = true;
}
void unhide() {
		_hidden = false;
}
bool isHidden() const {
		return _hidden;
}

void dumpPoints(std::string time_now) {
		InputPointDense *originalInput = (InputPointDense *)pointData;

		PointCloud::Ptr cloud(new PointCloud);

		for (int idx = 2; idx < pointNum - 2; idx++) {
				float x = originalInput[idx].x;
				float y = originalInput[idx].y;
				float depth = originalInput[idx].depth;

				PointT point;
				point.x = x;
				point.y = y;
				point.z = depth;
				point.r = originalInput[idx].color[0];
				point.g = originalInput[idx].color[1];
				point.b = originalInput[idx].color[2];
				cloud->push_back(point);
		}

		pcl::io::savePCDFileASCII("cloud" + time_now + ".pcd", *cloud);
}

void updatePoints(Keyframe *newFrame) {
		if (pointData == 0) {
				pointData = new unsigned char[pointNum * sizeof(InputPointDense)];
		}

		memcpy(pointData, newFrame->pointData, pointNum * sizeof(InputPointDense));

		needsUpdate = true;
}

void computeVbo() {
		assert(!(hasVbo && !needsUpdate));

		if (hasVbo && needsUpdate) {
				glDeleteBuffers(1, &vbo);
				points = 0;
		}

		MyVertex *tmpBuffer = new MyVertex[pointNum];
		float my_scale = lsd_slam::Conf().scale; // camToWorld.scale();
		float my_scale4 = my_scale * my_scale;
		my_scale4 *= my_scale4;
		// float my_scaledTH = lsd_slam::Conf().scaledTh;
		float my_scaledTH = 0.5;
		float my_absTH = lsd_slam::Conf().absTh;
		int my_minNearSupport = lsd_slam::Conf().nearSupport;
		int my_sparsifyFactor = lsd_slam::Conf().sparisityFactor;

		InputPointDense *originalInput = (InputPointDense *)pointData;

		float fxi = 1 / fx;
		float fyi = 1 / fy;
		float cxi = -cx / fx;
		float cyi = -cy / fy;

		int runningSparistyFailureCount = 0;
		int runningabsTHFailureCount = 0;
		int runningscaledTHFailureCount = 0;
		int runningNearSupportFailureCount = 0;

		PointCloud::Ptr cloud(new PointCloud);
		PointCloud::Ptr cloud_filtered(new PointCloud);
		float x;
		float y;
		float depth;

		for (int idx = 2; idx < pointNum - 2; idx++) {
				bool fail = false;
				x = originalInput[idx].x;
				y = originalInput[idx].y;
				depth = originalInput[idx].depth;
				bool variance_filter = true;
				// bool variance_filter = lsd_slam::Conf().useVarianceFiltering;

				if (variance_filter) {
						float depth4 = depth * depth;

						float _x = originalInput[idx].xImg;
						float _y = originalInput[idx].xImg;

						depth4 *= depth4;

						if (depth <= 0)
								continue;

						if (my_sparsifyFactor > 1 && rand() % my_sparsifyFactor != 0) {
								fail = true;
								runningSparistyFailureCount++;
						}

						if (originalInput[idx].idepth_var * depth4 > my_scaledTH) {
								fail = true;
								runningscaledTHFailureCount++;
						} else {
						}

						if (my_minNearSupport > 1) {
								int nearSupport = 0;
								for (int dx = -1; dx < 2; dx++) {
										for (int dy = -1; dy < 2; dy++) {
												int _idx = idx + dx + dy;
												if (originalInput[_idx].depth > 0) {
														float diff = 1 / originalInput[_idx].depth - 1.0f / depth;
														if (diff * diff <
														    2 * my_scale * originalInput[idx].idepth_var) {
																nearSupport++;
														}
												}
										}
								}

								if (nearSupport < my_minNearSupport) {
										fail = true;
										runningNearSupportFailureCount++;
								}
						}
				}
				if (!fail) {
						if (lsd_slam::Conf().useVoxelFilter) {
								PointT point;
								point.x = x;
								point.y = y;
								point.z = depth;
								point.r = originalInput[idx].color[0];
								point.g = originalInput[idx].color[1];
								point.b = originalInput[idx].color[2];
								cloud->push_back(point);
						} else {
								tmpBuffer[points].point[0] = x;
								tmpBuffer[points].point[1] = y;
								tmpBuffer[points].point[2] = depth;
								tmpBuffer[points].color[3] = 100;
								tmpBuffer[points].color[2] = originalInput[idx].color[0];
								tmpBuffer[points].color[1] = originalInput[idx].color[1];
								tmpBuffer[points].color[0] = originalInput[idx].color[2];
						}

						points++;
						keptPointCount++;
				}
		}
		if (lsd_slam::Conf().useVoxelFilter) {
				pcl::VoxelGrid<PointT> sor;
				sor.setInputCloud(cloud);
				sor.setLeafSize(lsd_slam::Conf().pclLeafSize,
				                lsd_slam::Conf().pclLeafSize,
				                lsd_slam::Conf().pclLeafSize);
				sor.filter(*cloud_filtered);

				for (int p = 0; p < cloud_filtered->size(); p++) {
						PointT point = cloud_filtered->at(p);
						tmpBuffer[p].point[0] = point.x;
						tmpBuffer[p].point[1] = point.y;
						tmpBuffer[p].point[2] = point.z;
						tmpBuffer[p].color[3] = 100;
						tmpBuffer[p].color[2] = point.b;
						tmpBuffer[p].color[1] = point.g;
						tmpBuffer[p].color[0] = point.r;
				}
		}

		LOGF_IF(INFO, lsd_slam::Conf().printGUIinfo,
		        "running sparisty failure count: %i, running absTH failure "
		        "count: %i, running scaledTH failure count: %i, "
		        "running near support failure count: %i",
		        runningSparistyFailureCount, runningabsTHFailureCount,
		        runningscaledTHFailureCount, runningNearSupportFailureCount);

		glGenBuffers(1, &vbo);
		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glBufferData(GL_ARRAY_BUFFER, sizeof(MyVertex) * points, tmpBuffer,
		             GL_STATIC_DRAW);

		// delete[] tmpBuffer;

		// delete[] pointData;

		// pointData = 0;

		hasVbo = true;

		needsUpdate = false;
}

int drawPoints() {

		if (_hidden)
				return 0;

		assert(hasVbo);
		GLfloat mi(0.0);
		glPointParameterf(GL_POINT_SIZE_MIN, mi);
		glEnable(GL_PROGRAM_POINT_SIZE);
		GLfloat s(lsd_slam::Conf().pointcloudSize);
		glPointSize(s);
		// GLfloat *min;
		// glGetFloatv(GL_POINT_SIZE_MIN, min);
		// ROS_WARN("min: %f", *min);

		glPushMatrix();

		Sophus::Matrix4f m = camToWorld.matrix();
		glMultMatrixf((GLfloat *)m.data());

		glBindBuffer(GL_ARRAY_BUFFER, vbo);
		glVertexPointer(3, GL_FLOAT, sizeof(MyVertex), 0);
		glColorPointer(4, GL_UNSIGNED_BYTE, sizeof(MyVertex),
		               (const void *)(3 * sizeof(float)));

		glEnableClientState(GL_VERTEX_ARRAY);
		glEnableClientState(GL_COLOR_ARRAY);

		glDrawArrays(GL_POINTS, 0, points);

		glDisableClientState(GL_COLOR_ARRAY);
		glDisableClientState(GL_VERTEX_ARRAY);

		glBindBuffer(GL_ARRAY_BUFFER, 0);

		glPopMatrix();

		return points;
}

void drawCamera() {

		if (_hidden)
		{
				LOG(INFO) << "hidden";
				return;
		}
		// LOG(INFO) << "not hidden\n" << camToWorld.matrix3x4();

		glPushMatrix();
		float size = 0.2;
		Sophus::Matrix4f m = camToWorld.matrix();

		// LOG(INFO) << "kf pose\n" << m;

		glMultMatrixf((GLfloat *)m.data());

		glColor3f(1, 0, 0);
		glBegin(GL_LINES);
		glVertex3f(0, 0, 0);
		glVertex3f(size * (0 - cx) / fx, size * (0 - cy) / fy, size);
		glVertex3f(0, 0, 0);
		glVertex3f(size * (0 - cx) / fx, size * (height - 1 - cy) / fy, size);
		glVertex3f(0, 0, 0);
		glVertex3f(size * (width - 1 - cx) / fx, size * (height - 1 - cy) / fy,
		           0.05);
		glVertex3f(0, 0, 0);
		glVertex3f(size * (width - 1 - cx) / fx, size * (0 - cy) / fy, size);
		glVertex3f(size * (width - 1 - cx) / fx, size * (0 - cy) / fy, size);
		glVertex3f(size * (width - 1 - cx) / fx, size * (height - 1 - cy) / fy,
		           size);
		glVertex3f(size * (width - 1 - cx) / fx, size * (height - 1 - cy) / fy,
		           size);
		glVertex3f(size * (0 - cx) / fx, size * (height - 1 - cy) / fy, size);
		glVertex3f(size * (0 - cx) / fx, size * (height - 1 - cy) / fy, size);
		glVertex3f(size * (0 - cx) / fx, size * (0 - cy) / fy, size);
		glVertex3f(size * (0 - cx) / fx, size * (0 - cy) / fy, size);
		glVertex3f(size * (width - 1 - cx) / fx, size * (0 - cy) / fy, size);
		glEnd();
		glPopMatrix();
		glColor3f(1, 1, 1);
}

void reset() {
		glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
}

void setPointSize(float size) {
		pointSize = size;
}

int id;
int initId;
uint64_t time;
bool isKeyframe;

Sophus::Sim3f camToWorld;

float fx;
float fy;
float cx;
float cy;
int height;
int width;

unsigned char *pointData;

bool hasVbo;
GLuint vbo;
int points;
bool needsUpdate;

float pointSize;
int pointNum;

int keptPointCount = 0;

float scale;
float scaledTh;
float absTh;
int nearSupport;
int sparisityFactor;

bool _hidden;
};

#endif /* KEYFRAME_H_ */
