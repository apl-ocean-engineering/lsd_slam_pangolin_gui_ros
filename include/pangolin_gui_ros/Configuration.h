
#include <g3log/g3log.hpp> // Provides CHECK() macros
#include <opencv2/core/core.hpp>

#ifdef USE_ZED
#include <zed/Camera.hpp>
#endif

#include <libvideoio/types/Camera.h>
#include <libvideoio/types/ImageSize.h>

#pragma once

namespace lsd_slam {

using libvideoio::Camera;
using libvideoio::ImageSize;

class Configuration;

Configuration &Conf();

// Slow migration from the global settings.[h,cpp] model to a Configuration
// object.
class Configuration {
public:
  friend Configuration &Conf();

  // Does additional validation on sz
  const ImageSize &setSlamImageSize(const ImageSize &sz);
  ImageSize slamImageSize;
  //  Camera camera;

  // GUI paramaters
  float pointcloudSize;
  bool printGUIinfo;
  float scale;
  bool setScale;
  float scaledTh;
  bool setscaledTh;
  float absTh;
  bool setabsTh;
  float nearSupport;
  bool setnearSupport;
  float sparisityFactor;
  bool setsparisityFactor;
  bool onSceenInfoDisplay;
  bool dumpMap;
  bool doFullReConstraintTrack;
  bool useVarianceFiltering;
  bool useVoxelFilter;
  float pclLeafSize;

private:
  // Private constructor.  User shouldn't make their own copy of Configuration
  Configuration();
};

} // namespace lsd_slam
