#pragma once

#include <string>

#include "ThreadMutexObject.h"
#include "pangolin_gui_ros/Keyframe.h"

namespace PangolinGui {
namespace StateSaver {

extern bool SaveState(const std::string &filename,
                      ThreadMutexObject<std::map<int, Keyframe *>> &keyframes);

}
} // namespace PangolinGui
