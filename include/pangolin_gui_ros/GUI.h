/*
 * GUI.h
 *
 *  Created on: 15 Aug 2014
 *      Author: thomas
 */

#pragma once

#define GLM_FORCE_RADIANS

#include <glm/glm.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <map>
#include <pangolin/gl/gl.h>
#include <pangolin/gl/gldraw.h>
#include <pangolin/pangolin.h>

#include "libvideoio/types/Camera.h"
#include "libvideoio/types/ImageSize.h"

#include "Keyframe.h"
#include "ThreadMutexObject.h"

#define GL_GPU_MEM_INFO_CURRENT_AVAILABLE_MEM_NVX 0x9049

class GUI {
public:
GUI(const libvideoio::ImageSize &sz, const libvideoio::Camera &camera);

virtual ~GUI();

void initImages();

void initButtons();

void preCall();

void postCall();

void addKeyframe(Keyframe *newFrame);

void updateLiveImage(unsigned char *data);
void updateDepthImage(unsigned char *data);
void drawPose();

void updateKeyframePoses(GraphFramePose *framePoseData, int num);

void drawKeyframes();

void drawImages();

void updateFrameNumber(int frameNumber);

void updatePointsNumber(int totalPointsNumber);

// The master roll-up of all of the updating
void update(void);

void setRescaleFactor(float rescaleFactor);

void dumpPoints(std::string time_now);

ThreadMutexObject<Sophus::Sim3f> pose;

int fx, fy, cx, cy, width, height;
bool cameraIntsSet = false;

protected:
void saveStateCallback();
void resetPointCloudCallback();

private:
libvideoio::ImageSize _imageSize;
libvideoio::Camera _camera;

void drawGrid();

pangolin::GlTexture *liveImg;
pangolin::GlTexture *depthImg;

ThreadMutexObject<unsigned char *> liveImgBuffer;
ThreadMutexObject<unsigned char *> depthImgBuffer;

pangolin::Var<int> *frameNumber;

pangolin::Var<int> *totalPoints;

pangolin::Var<std::function<void(void)> > *_saveState;
pangolin::Var<std::function<void(void)> > *_resetPointCloud;

pangolin::OpenGlRenderState s_cam;

ThreadMutexObject<std::map<int, Keyframe *> > keyframes;

float _reScaleFactor;


};
