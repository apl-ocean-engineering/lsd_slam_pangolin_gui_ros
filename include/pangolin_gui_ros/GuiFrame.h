#include "libvideoio/types/Camera.h"
#include "libvideoio/types/ImageSize.h"

#include <mutex>

using libvideoio::Camera;
using libvideoio::ImageSize;

class GuiFrame {
public:
  GuiFrame(const int id, const Camera camera, const ImageSize imsize,
           const double timestamp, unsigned char *image);

  int id();
  Camera camera();
  ImageSize imsize();
  double timestamp();
  unsigned char *image();

  std::mutex frameMutex;

private:
  int _id;
  Camera _camera;
  ImageSize _imsize;
  double _timestamp;
  unsigned char *_image;
};
