#pragma once
#include <ros/ros.h>

#include "slam_msgs/frameMsg.h"
#include "slam_msgs/framePoseData.h"
#include "slam_msgs/keyframeGraphMsg.h"
#include "slam_msgs/keyframeMsg.h"

#include <geometry_msgs/PoseStamped.h>
#include <sensor_msgs/Image.h>
#include <sensor_msgs/PointCloud2.h>

#include <boost/foreach.hpp>
#include <boost/thread/thread.hpp>

#include "pangolin_gui_ros/PangolinOutputIOWrapper.h"

#include <opencv2/highgui/highgui.hpp>

#include <cv_bridge/cv_bridge.h>

#include "std_srvs/SetBool.h"

#include <dynamic_reconfigure/server.h>
#include <pangolin_gui_ros/guiCFGConfig.h>

namespace lsd_slam {

class ROSInputThread {
public:
  ROSInputThread(const std::shared_ptr<PangolinOutputIOWrapper> oWrap,
                 GUI &_gui);
  ~ROSInputThread();

  // Callbacks and subscriptions
  void keyFrameImageCallback(const slam_msgs::frameMsg::ConstPtr &kfMsg,
                             const sensor_msgs::ImageConstPtr &img);

  void debugImageCallback(const sensor_msgs::ImageConstPtr &img);
  void keyFrameGraphCallback(const slam_msgs::keyframeGraphMsg::ConstPtr &msg);
  void framePoseDataCallback(const slam_msgs::framePoseData::ConstPtr &msg);
  void poseCallback(const geometry_msgs::PoseStampedConstPtr &msg);
  bool fullReset(std_srvs::SetBool::Request &req,
                 std_srvs::SetBool::Response &res);

  void dynamicReconfigureCallback(pangolin_gui_ros::guiCFGConfig &config,
                                  uint32_t level) {
    Conf().scale = config.pointcloud_scale;
    Conf().scaledTh = config.scaled_th;
    Conf().absTh = config.scaled_th;
    Conf().nearSupport = config.near_support;
    Conf().sparisityFactor = config.sparisity_factor;
    Conf().pclLeafSize = config.pcl_leaf_size;
    Conf().useVoxelFilter = config.use_voxel_filter;
    Conf().useVarianceFiltering = config.use_variance_filtering;
  }

  void run();

  std::shared_ptr<PangolinOutputIOWrapper> outputWrapper;

  ThreadMutexObject<bool> inputDone;
  ThreadSynchronizer inputReady;

  bool flipDebug;

  // protected:
  // std::shared_ptr<lsd_slam::OutputIOWrapper> output;

private:
  Sophus::Sim3f _pose;
  float rescale;
  bool fullResetRequested;

  GUI &gui;
  std::vector<uint16_t> frameIds;
  std::vector<float> frameDataVect;
};
} // namespace lsd_slam
