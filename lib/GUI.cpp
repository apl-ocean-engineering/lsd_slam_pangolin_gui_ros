/*
 * GUI.cpp
 *
 *  Created on: 17 Oct 2014
 *      Author: thomas
 */

#include "pangolin_gui_ros/GUI.h"
#include "pangolin_gui_ros/StateSaver.h"

#include <pangolin/display/display.h>

using namespace libvideoio;

GUI::GUI(const ImageSize &sz, const Camera &camera)
    : _imageSize(sz), _camera(camera), liveImg(NULL), depthImg(NULL),
      liveImgBuffer(NULL), depthImgBuffer(NULL), _reScaleFactor(0.5),
      _saveState(NULL) {
  const int initialWidth = 800, initialHeight = 800;

  pangolin::CreateWindowAndBind(
      "Main", initialWidth, initialHeight); //, GLUT_DOUBLE | GLUT_RGBA |
                                            // GLUT_DEPTH | GLUT_MULTISAMPLE);

  glDisable(GL_MULTISAMPLE);
  glEnable(GL_PROGRAM_POINT_SIZE_EXT);

  glEnable(GL_DEPTH_TEST);

  s_cam = pangolin::OpenGlRenderState(
      pangolin::ProjectionMatrix(640, 480, 420, 420, 320, 240, 0.1, 1000),
      pangolin::ModelViewLookAt(0, 0, -1, 0, 0, 0, pangolin::AxisY));

  pangolin::Display("cam")
      .SetBounds(0, 1.0f, 0, 1.0f, -640 / 480.0)
      .SetHandler(new pangolin::Handler3D(s_cam));

  pangolin::Display("LiveImage").SetAspect(_imageSize.aspectRatio());
  pangolin::Display("DepthImage").SetAspect(_imageSize.aspectRatio());

  pangolin::Display("multi")
      .SetBounds(pangolin::Attach::Pix(0), 1 / 4.0f, pangolin::Attach::Pix(180),
                 1.0)
      .SetLayout(pangolin::LayoutEqualHorizontal)
      .AddDisplay(pangolin::Display("LiveImage"))
      .AddDisplay(pangolin::Display("DepthImage"));

  pangolin::CreatePanel("ui").SetBounds(0.0, 1.0, 0.0,
                                        pangolin::Attach::Pix(180));

  initButtons();
  initImages();
}

GUI::~GUI() {
  if (depthImg)
    delete depthImg;

  if (depthImgBuffer.getValue())
    delete[] depthImgBuffer.getValue();

  if (liveImg)
    delete liveImg;

  if (liveImgBuffer.getValue())
    delete[] liveImgBuffer.getValue();

  {
    std::lock_guard<std::mutex> lock(keyframes.mutex());

    for (std::map<int, Keyframe *>::iterator i =
             keyframes.getReference().begin();
         i != keyframes.getReference().end(); ++i) {
      delete i->second;
    }

    keyframes.getReference().clear();
  }

  if (totalPoints)
    delete totalPoints;
  if (frameNumber)
    delete frameNumber;
  if (_saveState)
    delete _saveState;
}

void GUI::initButtons() {

  frameNumber = new pangolin::Var<int>("ui.Key Frame", 0);

  totalPoints = new pangolin::Var<int>("ui.Total points", 0);

  _saveState = new pangolin::Var<std::function<void(void)>>(
      "ui.Save state", std::bind(&GUI::saveStateCallback, this));
  _resetPointCloud = new pangolin::Var<std::function<void(void)>>(
      "ui.(R)eset point cloud", std::bind(&GUI::resetPointCloudCallback, this));

  // Keyboard shortcuts
  pangolin::RegisterKeyPressCallback(
      'r', std::bind(&GUI::resetPointCloudCallback, this));
}
void GUI::initImages() {
  depthImg = new pangolin::GlTexture(_imageSize.width, _imageSize.height,
                                     GL_RGB, true, 0, GL_RGB, GL_UNSIGNED_BYTE);
  depthImgBuffer.assignValue(new unsigned char[_imageSize.area() * 3]);

  liveImg = new pangolin::GlTexture(_imageSize.width, _imageSize.height, GL_RGB,
                                    true, 0, GL_RGB, GL_UNSIGNED_BYTE);
  liveImgBuffer.assignValue(new unsigned char[_imageSize.area() * 3]);
}

void GUI::update(void) {
  // LOG(INFO) << "update";
  preCall();
  drawKeyframes();
  drawImages();
  postCall();
}

void GUI::updateDepthImage(unsigned char *data) {
  std::lock_guard<std::mutex> lock(depthImgBuffer.mutex());
  memcpy(depthImgBuffer.getReference(), data, _imageSize.area() * 3);
}

void GUI::updateLiveImage(unsigned char *data) {
  std::lock_guard<std::mutex> lock(liveImgBuffer.mutex());
  memcpy(liveImgBuffer.getReference(), data, _imageSize.area() * 3);
}

void GUI::updateFrameNumber(int fn) { frameNumber->operator=(fn); }

void GUI::updatePointsNumber(int totalPointsNumber) {
  totalPoints->operator=(totalPointsNumber);
}

void GUI::addKeyframe(Keyframe *newFrame) {
  std::lock_guard<std::mutex> lock(keyframes.mutex());

  // Exists
  if (keyframes.getReference().find(newFrame->id) !=
      keyframes.getReference().end()) {
    keyframes.getReference()[newFrame->id]->updatePoints(newFrame);

    delete newFrame;
  } else {
    newFrame->initId = keyframes.getReference().size();
    keyframes.getReference()[newFrame->id] = newFrame;
  }
}

void GUI::drawPose() {
  // LOG(INFO) << "DRAW POSE: " << cameraIntsSet;
  if (!cameraIntsSet) {
    return;
  }
  //
  // LOG(INFO) << "pose\n" << pose.matrix3x4();

  glEnable(GL_MULTISAMPLE);
  glHint(GL_MULTISAMPLE_FILTER_HINT_NV, GL_NICEST);
  LOG(INFO) << width << " " << height << " " << cx << " " << cy << " " << fx
            << " " << fy;
  glPushMatrix();
  float size = 0.2;

  glMultMatrixf((GLfloat *)pose.getReference().matrix().data());

  glColor3f(1, 1, 1);
  glBegin(GL_LINES);
  glVertex3f(0, 0, 0);
  glVertex3f(size * (0 - cx) / fx, size * (0 - cy) / fy, size);
  glVertex3f(0, 0, 0);
  glVertex3f(size * (0 - cx) / fx, size * (height - 1 - cy) / fy, size);
  glVertex3f(0, 0, 0);
  glVertex3f(size * (width - 1 - cx) / fx, size * (height - 1 - cy) / fy, 0.05);
  glVertex3f(0, 0, 0);
  glVertex3f(size * (width - 1 - cx) / fx, size * (0 - cy) / fy, size);
  glVertex3f(size * (width - 1 - cx) / fx, size * (0 - cy) / fy, size);
  glVertex3f(size * (width - 1 - cx) / fx, size * (height - 1 - cy) / fy, size);
  glVertex3f(size * (width - 1 - cx) / fx, size * (height - 1 - cy) / fy, size);
  glVertex3f(size * (0 - cx) / fx, size * (height - 1 - cy) / fy, size);
  glVertex3f(size * (0 - cx) / fx, size * (height - 1 - cy) / fy, size);
  glVertex3f(size * (0 - cx) / fx, size * (0 - cy) / fy, size);
  glVertex3f(size * (0 - cx) / fx, size * (0 - cy) / fy, size);
  glVertex3f(size * (width - 1 - cx) / fx, size * (0 - cy) / fy, size);
  glEnd();
  glPopMatrix();

  // glPushMatrix();
  //
  // Eigen::Matrix4f m;
  // m << 0, 0, 1, 0, -1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 1;
  // glMultTransposeMatrixf((float *)m.data());
  //
  // glLineWidth(1);
  //
  // glBegin(GL_LINES);
  //
  // // Draw a larger grid around the outside..
  // double dGridInterval = 0.1;
  //
  // double dMin = -100.0 * dGridInterval;
  // double dMax = 100.0 * dGridInterval;
  //
  // double height = -4;
  //
  // for (int x = -10; x <= 10; x += 1) {
  //      if (x == 0)
  //              glColor3f(1, 1, 1);
  //      else
  //              glColor3f(0.3, 0.3, 0.3);
  //      glVertex3d((double)x * 10 * dGridInterval, dMin, height);
  //      glVertex3d((double)x * 10 * dGridInterval, dMax, height);
  // }
  //
  // for (int y = -10; y <= 10; y += 1) {
  //      if (y == 0)
  //              glColor3f(1, 1, 1);
  //      else
  //              glColor3f(0.3, 0.3, 0.3);
  //      glVertex3d(dMin, (double)y * 10 * dGridInterval, height);
  //      glVertex3d(dMax, (double)y * 10 * dGridInterval, height);
  // }
  //
  // glEnd();
  //
  // glBegin(GL_LINES);
  // dMin = -10.0 * dGridInterval;
  // dMax = 10.0 * dGridInterval;
  //
  // for (int x = -10; x <= 10; x ++) {
  //      if (x == 0)
  //              glColor3f(1, 1, 1);
  //      else
  //              glColor3f(0.5, 0.5, 0.5);
  //
  //      glVertex3d((double)x * dGridInterval, dMin, height);
  //      glVertex3d((double)x * dGridInterval, dMax, height);
  // }
  //
  // for (int y = -10; y <= 10; y ++) {
  //      if (y == 0)
  //              glColor3f(1, 1, 1);
  //      else
  //              glColor3f(0.5, 0.5, 0.5);
  //      glVertex3d(dMin, (double)y * dGridInterval, height);
  //      glVertex3d(dMax, (double)y * dGridInterval, height);
  // }
  //
  // glColor3f(1, 0, 0);
  // glVertex3d(0, 0, height);
  // glVertex3d(1, 0, height);
  // glColor3f(0, 1, 0);
  // glVertex3d(0, 0, height);
  // glVertex3d(0, 1, height);
  // glColor3f(1, 1, 1);
  // glVertex3d(0, 0, height);
  // glVertex3d(0, 0, height + 1);
  // glEnd();
  //
  // glPopMatrix();
  // glColor3f(1, 1, 1);

  // glDisable(L_MULTISAMPLE);

  // LOG(INFO) << "Published";
}

void GUI::updateKeyframePoses(GraphFramePose *framePoseData, int num) {
  std::lock_guard<std::mutex> lock(keyframes.mutex());
  LOG(INFO) << "updating key frame poses: " << num;
  for (int i = 0; i < num; i++) {

    if (keyframes.getReference().find(framePoseData[i].id) !=
        keyframes.getReference().end()) {
      LOG(INFO) << "Found match for frame " << framePoseData[i].id;
      // LOG(INFO) << "Frame " << framePoseData[i].id << " changing pose
      // from:\n"<< keyframes.getReference()[framePoseData[i].id] ->
      // camToWorld.translation(); LOG(INFO) << "to: "; for (int i =0; i<7; i
      // ++) {
      //      LOG(INFO) << framePoseData[i].camToWorld[i];
      // }

      memcpy(keyframes.getReference()[framePoseData[i].id]->camToWorld.data(),
             &framePoseData[i].camToWorld[0], sizeof(float) * 7);
      LOG(INFO) << "kf: " << framePoseData[i].id;
      LOG(INFO) << "updated pose \n"
                << (keyframes.getReference()[framePoseData[i].id]->camToWorld)
                       .matrix3x4();
    } else {
      LOG(INFO) << "Did not find match for frame " << framePoseData[i].id;
    }
  }
}

//== Actual draw/render functions ==

void GUI::preCall() {
  glClearColor(0.05, 0.05, 0.3, 0.0f);
  glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

  pangolin::Display("cam").Activate(s_cam);

  // drawGrid();
  drawPose();
}

void GUI::drawImages() {
  {
    std::lock_guard<std::mutex> lock(depthImgBuffer.mutex());
    depthImg->Upload(depthImgBuffer.getReference(), GL_RGB, GL_UNSIGNED_BYTE);
  }

  pangolin::Display("DepthImage").Activate();
  depthImg->RenderToViewport(true);

  {
    std::lock_guard<std::mutex> lock(liveImgBuffer.mutex());
    liveImg->Upload(liveImgBuffer.getReference(), GL_RGB, GL_UNSIGNED_BYTE);
  }

  pangolin::Display("LiveImage").Activate();
  liveImg->RenderToViewport(true);
}

void GUI::drawKeyframes() {
  std::lock_guard<std::mutex> lock(keyframes.mutex());

  glEnable(GL_MULTISAMPLE);
  glHint(GL_MULTISAMPLE_FILTER_HINT_NV, GL_NICEST);
  int count = 0;
  int totalPointsNumber = 0;
  for (std::map<int, Keyframe *>::iterator i = keyframes.getReference().begin();
       i != keyframes.getReference().end(); ++i) {

    // LOG(INFO) << "drawKeyframes: " << count;
    // Don't render first five, according to original code
    // if(i->second->initId >= 5)
    // {

    count++;
    if (!i->second->hasVbo || i->second->needsUpdate) {
      // LOG(WARNING) << "updating " << count;
      i->second->computeVbo();
      i->second->keptPointCount;
    }
    totalPointsNumber += i->second->drawPoints();
    i->second->drawCamera();

    // }
  }
  updatePointsNumber(totalPointsNumber);

  glDisable(GL_MULTISAMPLE);
}

void GUI::drawGrid() {
  // set pose
  glPushMatrix();

  Eigen::Matrix4f m;
  m << 0, 0, 1, 0, -1, 0, 0, 0, 0, -1, 0, 0, 0, 0, 0, 1;
  glMultTransposeMatrixf((float *)m.data());

  glLineWidth(1);

  glBegin(GL_LINES);

  // Draw a larger grid around the outside..
  double dGridInterval = 0.1;

  double dMin = -100.0 * dGridInterval;
  double dMax = 100.0 * dGridInterval;

  double height = -4;

  for (int x = -10; x <= 10; x += 1) {
    if (x == 0)
      glColor3f(1, 1, 1);
    else
      glColor3f(0.3, 0.3, 0.3);
    glVertex3d((double)x * 10 * dGridInterval, dMin, height);
    glVertex3d((double)x * 10 * dGridInterval, dMax, height);
  }

  for (int y = -10; y <= 10; y += 1) {
    if (y == 0)
      glColor3f(1, 1, 1);
    else
      glColor3f(0.3, 0.3, 0.3);
    glVertex3d(dMin, (double)y * 10 * dGridInterval, height);
    glVertex3d(dMax, (double)y * 10 * dGridInterval, height);
  }

  glEnd();

  glBegin(GL_LINES);
  dMin = -10.0 * dGridInterval;
  dMax = 10.0 * dGridInterval;

  for (int x = -10; x <= 10; x++) {
    if (x == 0)
      glColor3f(1, 1, 1);
    else
      glColor3f(0.5, 0.5, 0.5);

    glVertex3d((double)x * dGridInterval, dMin, height);
    glVertex3d((double)x * dGridInterval, dMax, height);
  }

  for (int y = -10; y <= 10; y++) {
    if (y == 0)
      glColor3f(1, 1, 1);
    else
      glColor3f(0.5, 0.5, 0.5);
    glVertex3d(dMin, (double)y * dGridInterval, height);
    glVertex3d(dMax, (double)y * dGridInterval, height);
  }

  glColor3f(1, 0, 0);
  glVertex3d(0, 0, height);
  glVertex3d(1, 0, height);
  glColor3f(0, 1, 0);
  glVertex3d(0, 0, height);
  glVertex3d(0, 1, height);
  glColor3f(1, 1, 1);
  glVertex3d(0, 0, height);
  glVertex3d(0, 0, height + 1);
  glEnd();

  glPopMatrix();
}

void GUI::postCall() {

  pangolin::FinishFrame();

  glFinish();
}

void GUI::setRescaleFactor(float rescaleFactor) {
  _reScaleFactor = rescaleFactor;
}

void GUI::dumpPoints(std::string time_now) {

  // Construct filename from current datetime
  std::string pcdFilename("pointcloud");
  pcdFilename += time_now;
  pcdFilename += ".pcd";

  PangolinGui::StateSaver::SaveState(pcdFilename, keyframes);
}

//=== Callbacks ===
void GUI::saveStateCallback() {
  LOG(INFO) << "In saveStateCallback";

  time_t rawtime;
  struct tm *timeinfo;

  time(&rawtime);
  timeinfo = gmtime(&rawtime);
  char dateStr[80];

  strftime(dateStr, 79, "%Y%m%d-%H%M%S", timeinfo);

  dumpPoints(dateStr);
}

void GUI::resetPointCloudCallback() {
  std::lock_guard<std::mutex> guard(keyframes.mutex());
  for (const auto kf_pair : keyframes.getReference()) {
    kf_pair.second->hide();
  }
  initButtons();
  initImages();
  drawImages();
}
