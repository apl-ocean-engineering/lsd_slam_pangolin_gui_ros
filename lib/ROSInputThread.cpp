#include "pangolin_gui_ros/ROSInputThread.h"

#include <pcl/common/common_headers.h>
#include <pcl/console/parse.h>
#include <pcl/features/normal_3d.h>
#include <pcl/filters/passthrough.h>
#include <pcl/io/pcd_io.h>
#include <pcl/point_types.h>
#include <pcl/visualization/pcl_visualizer.h>
#include <pcl_ros/point_cloud.h>

static const std::string OPENCV_WINDOW = "Image window";

namespace lsd_slam {

ROSInputThread::ROSInputThread(
		const std::shared_ptr<PangolinOutputIOWrapper> oWrap, GUI &_gui)
		: outputWrapper(oWrap), fullResetRequested(false), gui(_gui),
		flipDebug(false) {
}

ROSInputThread::~ROSInputThread() {
		cv::destroyWindow(OPENCV_WINDOW);
}

void ROSInputThread::keyFrameImageCallback(
		const slam_msgs::frameMsg::ConstPtr &fMsg,
		const sensor_msgs::ImageConstPtr &img) {

		LOG(INFO) << "keyFrameImageCallback";


		LOG(INFO) << img->width << " " << img->height;

		// KF callback
		int id = fMsg->id;
		// LOG(INFO) << "Key frame callback: " << id;
		Camera camera = Camera(fMsg->kf.fx, fMsg->kf.fy, fMsg->kf.cx, fMsg->kf.cy);
		ImageSize imsize(fMsg->kf.width, fMsg->kf.height);
		double ts = fMsg->time;
		// Img callback
		cv::Mat callbackImage = cv_bridge::toCvShare(img, "rgb8")->image;

		sensor_msgs::PointCloud2 pcRos = fMsg->kf.pointcloud;
		PointCloud pc;
		pcl::fromROSMsg(pcRos, pc);

		// Eigen::Vector3f t(fMsg->pose.position.x, fMsg->pose.position.y,
		//                   fMsg->pose.position.z);
		// LOG(INFO) << "t\n" << t;

		std::vector<float> idepthVar = fMsg->kf.idepthVar;

		std::shared_ptr<GuiFrame> guiFrame(
				new GuiFrame(id, camera, imsize, 0.0, callbackImage.data));
		//
		LOG(INFO) << "pose: " << _pose.matrix3x4();
		//
		outputWrapper->publishKeyframe(guiFrame, pc, idepthVar, _pose);
		callbackImage.convertTo(callbackImage, CV_8UC1);
		unsigned char *data;

		data = callbackImage.data;
		outputWrapper->updateLiveImage(data);
		outputWrapper->updateFrameNumber(fMsg->id);
}

void ROSInputThread::keyFrameGraphCallback(
		const slam_msgs::keyframeGraphMsg::ConstPtr &msg) {
}

void ROSInputThread::framePoseDataCallback(
		const slam_msgs::framePoseData::ConstPtr &msg) {
		LOG(INFO) << "framePoseDataCallback callback";
		int numFrames = (int)msg->numFrames;
		GraphFramePose buff[numFrames];

		frameIds = msg->id;
		frameDataVect = msg->frameData;
		for (int i = 0; i < numFrames; i++) {
				buff[i].id = (unsigned int)frameIds.at(i);
				for (int j = 0; j < 7; j++) {
						buff[i].camToWorld[j] = frameDataVect.at(i * 7 + j);
				}
		}

		GraphFramePose *framePoseData = buff;

		outputWrapper->updateKeyframePoses(framePoseData, numFrames);
}

void ROSInputThread::debugImageCallback(const sensor_msgs::ImageConstPtr &img) {
		// LOG(INFO) << "debugImageCallback callback" << img->width << " " << img->height;


		// LOG(INFO)

		cv::Mat callbackImage = cv_bridge::toCvShare(img, "rgb8")->image;
		callbackImage.convertTo(callbackImage, CV_8UC1);


		unsigned char *data;

		data = callbackImage.data;
		outputWrapper->updateDepthImage(data);
}

void ROSInputThread::poseCallback(
		const geometry_msgs::PoseStampedConstPtr &msg) {
		LOG(INFO) << "pose callback";

		Eigen::Quaternionf q(msg->pose.orientation.w, msg->pose.orientation.x,
		                     msg->pose.orientation.y, msg->pose.orientation.z);

		Eigen::Vector3f t(msg->pose.position.x, msg->pose.position.y,
		                  msg->pose.position.z);
		LOG(INFO) << "t\n" << t;
		LOG(INFO) << "q\n" << q.coeffs();
		Sophus::Sim3f pose(q, t);
		_pose = pose;
		outputWrapper->publishPose(pose);
}

bool ROSInputThread::fullReset(std_srvs::SetBool::Request &req,
                               std_srvs::SetBool::Response &res) {
		res.success = true;

		fullResetRequested = req.data;

		return true;
}

void ROSInputThread::run() {
		ros::Rate loop_rate(30);
		while (ros::ok()) {
				if (fullResetRequested) {
						// GUI gui(_imSize, _camera);
						// float pcSize = 1.0;
						// outputWrapper.reset(new PangolinOutputIOWrapper(gui, pcSize));
						// fullResetRequested = false;
				}
				outputWrapper->updateGui();
				loop_rate.sleep();
				ros::spinOnce();
		}
}

} // namespace lsd_slam
