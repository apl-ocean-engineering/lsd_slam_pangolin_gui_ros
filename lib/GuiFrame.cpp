#include "pangolin_gui_ros/GuiFrame.h"

GuiFrame::GuiFrame(const int id, const Camera camera, const ImageSize imsize,
                   const double timestamp, unsigned char *image)
    : _id(id), _camera(camera), _imsize(imsize), _timestamp(timestamp),
      _image(image) {}

int GuiFrame::id() { return _id; }

Camera GuiFrame::camera() { return _camera; }

ImageSize GuiFrame::imsize() { return _imsize; }

double GuiFrame::timestamp() { return _timestamp; }

unsigned char *GuiFrame::image() { return _image; }
