/*
 * PangolinOutputIOWrapper.cpp
 *
 *  Created on: 17 Oct 2014
 *      Author: thomas
 */

#include "pangolin_gui_ros/PangolinOutputIOWrapper.h"
#include "sophus/sim3.hpp"

namespace lsd_slam {

PangolinOutputIOWrapper::PangolinOutputIOWrapper(GUI &gui, float &pointSize)
		: publishLvl(0), _gui(gui), _pointSize(pointSize) {
}
PangolinOutputIOWrapper::PangolinOutputIOWrapper(GUI &gui)
		: publishLvl(0), _gui(gui), _pointSize(0.5) {
}
PangolinOutputIOWrapper::~PangolinOutputIOWrapper() {
}

void PangolinOutputIOWrapper::publishPose(const Sophus::Sim3f &pose) {
		_gui.pose.set(pose);
		// _gui.drawPose(pose);
}

void PangolinOutputIOWrapper::updateDepthImage(unsigned char *data) {
		_gui.updateDepthImage(data);
}
void PangolinOutputIOWrapper::updateLiveImage(unsigned char *data) {
		_gui.updateLiveImage(data);
}
void PangolinOutputIOWrapper::updateFrameNumber(int id) {
		_gui.updateFrameNumber(id);
}

void PangolinOutputIOWrapper::publishKeyframe(
		std::shared_ptr<GuiFrame> &frame, const PointCloud &inputPc,
		const std::vector<float> idepthvar, const Sophus::Sim3f &pose) {
		// LOG(INFO) << "Received keyFrame " << frame->id();

		Keyframe *fMsg = new Keyframe;

		fMsg->setPointSize(_pointSize);

		{
				std::lock_guard<std::mutex> lock(frame->frameMutex);

				// boost::shared_lock<boost::shared_mutex> lock = frame->frameMutex;

				fMsg->id = frame->id();
				fMsg->time = frame->timestamp();
				fMsg->isKeyframe = true;

				int w = frame->imsize().width;
				int h = frame->imsize().height;

				fMsg->camToWorld = pose.cast<float>();

				fMsg->fx = frame->camera().fx;
				fMsg->fy = frame->camera().fy;
				fMsg->cx = frame->camera().cx;
				fMsg->cy = frame->camera().cy;

				fMsg->width = w;
				fMsg->height = h;

				fMsg->pointData = new unsigned char[w * h * sizeof(InputPointDense)];

				_gui.fx = frame->camera().fx;
				_gui.fy = frame->camera().fy;
				_gui.cx = frame->camera().cx;
				_gui.cy = frame->camera().cy;
				_gui.width = w;
				_gui.height = h;
				_gui.cameraIntsSet = true;
				//
				// if (Conf().setScale)
				//   fMsg->scale = Conf().scale;
				// if (Conf().setscaledTh)
				//   fMsg->scaledTh = Conf().scaledTh;
				// if (Conf().setabsTh)
				//   fMsg->absTh = Conf().absTh;
				// if (Conf().setnearSupport)
				//   fMsg->nearSupport = Conf().nearSupport;
				// if (Conf().setsparisityFactor)
				//   fMsg->sparisityFactor = Conf().sparisityFactor;

				InputPointDense *pc = (InputPointDense *)fMsg->pointData;

				int pointNum = 0;
				int idx = 0;
				for (int yImg = 0; yImg < h; yImg++) {
						for (int xImg = 0; xImg < w; xImg++, idx++) {
								PointT point = inputPc.at(idx);
								if (point.z > 0) {
										pc[pointNum].xImg = xImg;
										pc[pointNum].yImg = yImg;
										pc[pointNum].x = point.x;
										pc[pointNum].y = point.y;
										// std::cout << point.z << std::endl;
										pc[pointNum].depth = point.z;
										pc[pointNum].color[0] = point.r;
										pc[pointNum].color[1] = point.g;
										pc[pointNum].color[2] = point.b;
										pc[pointNum].idepth_var = idepthvar.at(idx);

										pointNum++;
								}
						}
				}

				fMsg->pointNum = pointNum;
		}

		_gui.addKeyframe(fMsg);
}

void PangolinOutputIOWrapper::updateKeyframePoses(GraphFramePose *framePoseData,
                                                  const int num) {
		_gui.updateKeyframePoses(framePoseData, num);
}

void PangolinOutputIOWrapper::publishTrajectory(
		std::vector<Eigen::Matrix<float, 3, 1> > trajectory,
		std::string identifier) {
		// TODO
}

void PangolinOutputIOWrapper::updateGui() {
		_gui.update();
}

void PangolinOutputIOWrapper::dumpPoints(std::string time_now) {
		_gui.dumpPoints(time_now);
}

} // namespace lsd_slam
