
#include <fstream>

#include "pangolin_gui_ros/StateSaver.h"

namespace PangolinGui {
namespace StateSaver {

/// From the PCD documentation:
// The header entries must be specified precisely in the above order, that is:
//     VERSION
//     FIELDS
//     SIZE
//     TYPE
//     COUNT
//     WIDTH
//     HEIGHT
//     VIEWPOINT
//     POINTS
//     DATA

bool SaveState(const std::string &filename,
               ThreadMutexObject<std::map<int, Keyframe *>> &keyframes) {

  LOG(INFO) << "Saving state to: " << filename;

  std::ofstream outf(filename);

  if (!outf.is_open()) {
    LOG(WARNING) << "file not open";
    return false;
  }

  outf << "VERSION 0.7" << std::endl;
  outf << "FIELDS x y z rgb" << std::endl;
  outf << "SIZE 4 4 4 4" << std::endl; // Store all fields as floats
  outf << "TYPE F F F F" << std::endl;
  outf << "COUNT 1 1 1 1" << std::endl;

  unsigned int numPoints = 0;

  {
    std::lock_guard<std::mutex> guard(keyframes.mutex());

    for (const auto kf_pair : keyframes.getReference()) {
      const int kfNum = kf_pair.first;
      Keyframe *kf = kf_pair.second;

      // Do something useful with keyframe
      numPoints += kf->pointNum;
    }

    outf << "WIDTH " << numPoints << std::endl;
    outf << "HEIGHT 1" << std::endl;
    outf << "VIEWPOINT 0 0 0 1 0 0 0" << std::endl;

    outf << "POINTS " << numPoints << std::endl;
    outf << "DATA ascii" << std::endl;

    // iterate a second time
    for (const auto kf_pair : keyframes.getReference()) {
      const int kfNum = kf_pair.first;
      Keyframe *kf = kf_pair.second;

      // Do something useful with keyframe
      numPoints += kf->pointNum;

      // Who the F writes code like this?
      InputPointDense *inputPoints = (InputPointDense *)kf->pointData;

      for (unsigned int i = 0; i < kf->pointNum; i++) {
        // const auto &point = inputPoints[i];
        float x = inputPoints[i].x;
        float y = inputPoints[i].y;
        float depth = inputPoints[i].depth;
        // \todo  Should be a validity check here?
        // if( point.)

        uint32_t rgb = ((uint32_t)inputPoints[i].color[0] << 16 |
                        (uint32_t)inputPoints[i].color[1] << 8 |
                        (uint32_t)inputPoints[i].color[2]);
        const float rgbFloat = float(rgb);
        //
        // std::string outStr = "%f %f %f %f" % (x, y, depth, rgbFloat)l
        outf << x << " " << y << " " << depth << " " << rgbFloat << std::endl;
        // outf << x << " " << std::endl;
      }
    }
  }

  return true;
}

} // namespace StateSaver
} // namespace PangolinGui
